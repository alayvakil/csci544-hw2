import sys
import os
import math
import json
from pclasshead import classify
import codecs


sys.stdin = codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
sys.stdout = codecs.getwriter('latin-1')(sys.stdout.detach(), errors='ignore')

if(len(sys.argv) != 2 ):
	sys.stderr.write("USAGE : python percepclassify.py modelfile ----------------------------------TRY AGAIN\n")
	exit()


modelfile = open(sys.argv[1],"r")
full = modelfile.read()
dictofclass = json.loads(full)

for x in dictofclass:
		dictofclass[x]["probs"] = 0

for line in sys.stdin:
	argmax = classify(line , dictofclass)
	sys.stdout.write(argmax)
	sys.stdout.flush()




