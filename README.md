PART 1

USAGE : python perceplearn.py trainingfile modelfile [-h devfile]
USAGE : python percepclassify.py modelfile

PART 2 

USAGE : python poslearn.py trainingfile modelfile [-h devfile]
USAGE : python postag.py modelfile

PART 3

USAGE : python nerlearn.py trainingfile modelfile [-h devfile]
USAGE : python nertag.py modelfile

PART 4

    (5 pts) What is the accuracy of your part-of-speech tagger? 95.8%

    (5 pts) What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score?

    	
precision for B-PER

94.21364985163204

recall for B-PER

51.96399345335516

fscore for B-PER

66.9831223628692

precision for I-MISC

89.92805755395683

recall for I-MISC

95.565749235474

fscore for I-MISC

92.66123054114159

precision for I-LOC

85.1948051948052

recall for I-LOC

97.3293768545994

fscore for I-LOC

90.85872576177286

precision for B-ORG

91.43314651721377

recall for B-ORG

67.17647058823529

fscore for B-ORG

77.44998304510004

precision for B-LOC

77.89890981169475

recall for B-LOC

79.8780487804878

fscore for B-LOC

78.87606623181134

precision for I-ORG

86.76671214188268

recall for I-ORG

93.11859443631039

fscore for I-ORG

89.83050847457628

precision for B-MISC

80.078125

recall for B-MISC

46.06741573033708

fscore for B-MISC

58.48787446504993

precision for I-PER

94.99431171786121

recall for I-PER

97.20605355064028

fscore for I-PER

96.08745684695052


AVG FSCORE81.40437096615896



    (10 pts) What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is?

    		Accuracy for POS tagging using Naive Bayes classifier is 0.8660202402911411 . It is because the features were percieved as unrelated.
