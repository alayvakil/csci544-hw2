import sys
import os
import re
import json



if(len(sys.argv) != 5 and len(sys.argv) != 3 ):
	print ("USAGE : python nelearn.py trainingfile modelfile [-h devfile] ----------------------------------TRY AGAIN")
	exit()
if(len(sys.argv) == 3):
	train = sys.argv[1]
	dev = train 
else:
	train = sys.argv[1]
	dev = sys.argv[4]

outfile = open("temp_pos_train_out","w+",encoding='latin-1')
trainingfile = open(train,"r+",encoding='latin-1')
mem = 0 

for line in trainingfile:

	line = line.strip(' \n\t')
	words = line.split(' ')

	prev = "PEOF"
	# prevpos = "PEOF"
	prev2 = "PEOF"

	for i in range(0,len(words)-1):
		trip = words[i].split('/')


		outfile.write(trip[2] +" prev2_"+prev2+" prev_"+ prev + " " + trip[0]+" next_"+words[i+1].split('/')[0]+"\n")
		prev2 = prev
		prev = trip[2]
	
	trip = words[len(words)-1].split('/')
	outfile.write(trip[2] +" prev2_"+prev2+" prev_" + prev + " " + trip[0]+" next_LEOF\n")

trainingfile.close()
outfile.close()

outfile = open("temp_pos_dev_out","w+",encoding='latin-1')
devfile = open(dev,"r+",encoding='latin-1')

for line in devfile:

	line = line.strip(' \n\t')
	words = line.split(' ')

	prev = "PEOF"
	prev2 = "PEOF"

	for i in range(0,len(words)-1):
		
		typ =""

		trip = words[i].split('/')
		c = []

		if(len(trip)!=3):
			mix = ""
			for i in range(0,len(trip)-2):
				mix += trip[i]
			c.append(mix)
			c.append(trip[-2])
			c.append(trip[-1])
			trip = c


		f = re.search(r'[0-9]',trip[0])
		if f:
			typ += "0"
		f = re.search(r'^[A-Z].*',trip[0])
		if f:
			typ +="A"
		f = re.search(r'[a-z]',trip[0])
		if f:
			typ+="a"


		outfile.write(trip[2] +" prev2_"+prev2+" prev_"+ prev + " " + trip[0]+" next_"+words[i+1].split('/')[0]+" typ:"+ typ +"\n")
		prev2 = prev
		prev = trip[2]


	trip = words[len(words)-1].split('/')
	if(len(trip)!=3):
			mix = ""
			for i in range(0,len(trip)-2):
				mix += trip[i]
			c = []
			c.append(mix)
			c.append(trip[-2])
			c.append(trip[-1])
			trip = c

	typ =""

	f = re.search(r'[0-9]',trip[0])
	if f:
		typ += "0"
	if trip[0][0].isupper():
		typ +="A"
	f = re.search(r'[a-z]',trip[0])
	if f:
		typ+="a"

	outfile.write(trip[2] +" prev2_"+prev2+ " prev_"+ prev + " " + trip[0]+" next_LEOF "+"typ:"+typ+"\n")

devfile.close()
outfile.close()
command = "python3 perceplearn.py temp_pos_train_out "+sys.argv[2]+" -h temp_pos_dev_out"
os.system(command)