import sys
import os
import re

if(len(sys.argv) != 5 and len(sys.argv) != 3 ):
	print ("USAGE : python postrain.py trainingfile modelfile [-h devfile] ----------------------------------TRY AGAIN")
	exit()
if(len(sys.argv) == 3):
	train = sys.argv[1]
	dev = train 
else:
	train = sys.argv[1]
	dev = sys.argv[4]

outfile = open("temp_pos_train_out","w+",encoding='latin1')
trainingfile = open(train,"r+" ,encoding='latin1')

for line in trainingfile:

	line = line.strip(' \n\t')
	words = line.split(' ')

	prev = "PEOF"
	prev2 = "PEOF"

	for i in range(0,len(words)-1):
		pair = words[i].split('/')
		sufix = pair[0][-3:]
		outfile.write(pair[1] + " prev2_" +prev2+ " prev_"+ prev + " " + pair[0]+" next_"+words[i+1].split('/')[0]+" suffix_" + sufix + "\n")
		prev = pair[0]
		prev2 = pair[1]

	pair = words[len(words)-1].split('/')
	sufix = pair[0][-3:]
	outfile.write(pair[1] + " prev2_" +prev2+ " prev_"+ prev + " " + pair[0]+" suffix_" + sufix +" next_LEOF\n")

trainingfile.close()
outfile.close()

outfile = open("temp_pos_dev_out","w+",encoding='latin1')
devfile = open(dev,"r+",encoding='latin1')

for line in devfile:

	line = line.strip(' \n\t')
	words = line.split(' ')

	prev = "PEOF"
	prev2 = "PEOF"

	for i in range(0,len(words)-1):
		pair = words[i].split('/')
		sufix = pair[0][-3:]
		outfile.write(pair[1] + " prev2_" +prev2+ " prev_"+ prev + " " + pair[0]+" next_"+words[i+1].split('/')[0]+" suffix_" + sufix + "\n")
		prev = pair[0]
		prev2 = pair[1]

	pair = words[len(words)-1].split('/')
	sufix = pair[0][-3:]
	outfile.write(pair[1] + " prev2_" +prev2+ " prev_"+ prev + " " + pair[0]+" suffix_" + sufix +" next_LEOF\n")

devfile.close()
outfile.close()
command = "python3 perceplearn.py temp_pos_train_out "+sys.argv[2]+" -h temp_pos_dev_out"
os.system(command)