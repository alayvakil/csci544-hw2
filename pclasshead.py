import sys
import os
import math
import json

def classify(line , dictofclass):
		
	# print("CALLED FOR "+line)
	for elem in line.split(' '):

		elem = elem.strip(' \t\n\r')

		for classes in dictofclass:

			if elem in dictofclass[classes]["avg"]:

				dictofclass[classes]["probs"] += dictofclass[classes]["avg"][elem] 
			else:
				dictofclass[classes]["probs"] += dictofclass[classes]["avg"]["##unk##"]


	argmax = ""
	maxi = -sys.maxsize

	for classes in dictofclass:

		if(dictofclass[classes]["probs"] > maxi):
			maxi = dictofclass[classes]["probs"]
			argmax = classes

		dictofclass[classes]["probs"] = 0

	
	return argmax
