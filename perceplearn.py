import sys
import os
import json
import random

buff = ""
finalbuff = ""

if(len(sys.argv) != 5 and len(sys.argv) != 3 ):
	print ("USAGE : python perceplearn.py trainingfile modelfile [-h devfile] ----------------------------------TRY AGAIN")
	exit()
if(len(sys.argv) == 3):
	train = sys.argv[1]
	dev = train 
else:
	train = sys.argv[1]
	dev = sys.argv[4]

dictofclass = {}
featureweights = {}

trainingfile = open(train,encoding='latin1')
modelfile = open(sys.argv[2],"w+",encoding='latin1')

linesoffile = []

for line in trainingfile:

	words = line.split(' ' , 1)
	
	if words[0] not in dictofclass:
		c1 = { "name" : "" , "weight" : {} ,"temp" : 0 , "avg" : {}}
		c1["name"] = words[0]
		dictofclass[words[0]] = c1.copy()

	for elem in words[1].split(' '):

		elem = elem.strip(' \t\n\r')
		if(elem == ''):
			continue;

		if elem not in featureweights:
			featureweights[elem] = 0


featureweights["##unk##"] = 0 

for classes in dictofclass:
	dictofclass[classes]["weight"] = featureweights.copy()
	dictofclass[classes]["avg"] = featureweights.copy()

maxacc = -sys.maxsize
needed = {}

for iteration in range(1,20):

	trainingfile.close()
	trainingfile = open(train,encoding='latin1')
	linesoffile = trainingfile.readlines()
	random.shuffle(linesoffile)

	for line in linesoffile:

		words = line.split(' ', 1)

		for elem in words[1].split(' '):

			elem = elem.strip(' \t\n\r')
			if(elem == ''):
				continue;

			for classes in dictofclass:
				dictofclass[classes]["temp"] += dictofclass[classes]["weight"][elem] 

			
		argmax = ""
		maxi = -sys.maxsize

		for classes in dictofclass:

			if(dictofclass[classes]["temp"] > maxi):
				maxi = dictofclass[classes]["temp"]
				argmax = classes

			dictofclass[classes]["temp"] = 0


		if(argmax != words[0]):

			for elem in words[1].split(' '):

				elem = elem.strip(' \t\n\r')
				if(elem == ''):
					continue;
				dictofclass[argmax]["weight"][elem]-=1 
				dictofclass[words[0]]["weight"][elem]+=1

	for classes in dictofclass:
		for elem in featureweights:
			dictofclass[classes]["avg"][elem] += dictofclass[classes]["weight"][elem]  

	numerator = 0
	denominator = 0
	devfile = open(dev,"r",encoding='latin1')

	for line in devfile:

		words = line.split(' ', 1)

		for elem in words[1].split(' '):

			elem = elem.strip(' \t\n\r')


			for classes in dictofclass:


				if elem in dictofclass[classes]["avg"]:

					dictofclass[classes]["temp"] += dictofclass[classes]["avg"][elem] 
				else:
					dictofclass[classes]["temp"] += dictofclass[classes]["avg"]["##unk##"]


		argmax = ""
		maxi = -sys.maxsize

		for classes in dictofclass:

			if(dictofclass[classes]["temp"] > maxi):
				maxi = dictofclass[classes]["temp"]
				argmax = classes

			dictofclass[classes]["temp"] = 0

		if(argmax == words[0]):
			numerator+=1

		denominator+=1
		buff +=words[0] + " " + argmax + "\n" 

	devfile.close()

	print("Iteration " + str(iteration) +" Accuracy : "+ str(numerator/denominator)+"\n")

	if( (numerator/denominator) > maxacc ):
		needed = dictofclass
		finalbuff = buff
	buff = ""

modelfile.write(json.dumps(needed))
gold = open("gold.txt" , "w+" , encoding = 'latin1')
gold.write(finalbuff)
gold.close()
trainingfile.close()
modelfile.close()
