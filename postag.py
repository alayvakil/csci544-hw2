import sys
import os
import json
import random
import subprocess
from pclasshead import classify
import codecs

sys.stdin = codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
sys.stdout = codecs.getwriter('latin-1')(sys.stdout.detach(), errors='ignore')


if(len(sys.argv) != 2 ):
	sys.stderr.write("USAGE : python postag.py modelfile----------------------------------TRY AGAIN")
	exit()

modelfile = open(sys.argv[1],"r")
full = modelfile.read()
dictofclass = json.loads(full)


for x in dictofclass:
	dictofclass[x]["probs"] = 0

for line in sys.stdin:

	line = line.strip(' \n\t')
	words = line.split(' ')

	prev = "PEOF"
	prev2 = "PEOF"
	finalresult = ""
	query=""

	if(len(words)>1):
		for i in range(0,len(words)-1):
			sufix = words[i][-3:]
			query = "prev2_" +prev2+ " prev_"+ prev + " " + words[i] +" next_"+ words[i+1].split('/')[0]+" suffix_" + sufix +"\n"
			# print(query)
			# process = subprocess.Popen(['python3', 'percepclassify.py' , sys.argv[1] ] , shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
			# result = process.communicate(query.encode('utf-8'))
			result = classify(query,dictofclass)		
			prev2 = result
			# prev = result[0].decode('utf-8').strip('$')
			prev = words[i]
			finalresult = finalresult + words[i]+"/"+result +" "

	sufix = words[len(words)-1][-3:]
	query = "prev2_" +prev2+ " prev_"+ prev + " " + words[len(words)-1] +" next_"+" suffix_" + sufix + " next_LEOF"
	# print(query)
	# process = subprocess.Popen(['python3', 'percepclassify.py' , sys.argv[1] ] , shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
	# result = process.communicate(query.encode('utf-8'))
	result = classify(query,dictofclass)		
	prev = words[len(words)-1]
	# prev = result[0].decode('utf-8').strip('$')
	prev2 = result
	finalresult = finalresult + words[len(words)-1]+"/"+result +"\n"

	sys.stdout.write(finalresult)
	sys.stdout.flush()
	