import sys
import os
import json
import random
import subprocess
from pclasshead import classify
import codecs
import io

# sys.stdin = codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
# sys.stdout = codecs.getwriter('latin-1')(sys.stdout.detach(), errors='ignore')

if(len(sys.argv) != 2 ):
	print ("USAGE : python postag.py modelfile----------------------------------TRY AGAIN")
	exit()

modelfile = open(sys.argv[1],"r",encoding='latin-1')
full = modelfile.read()
dictofclass = json.loads(full)

for x in dictofclass:
	dictofclass[x]["probs"] = 0

with io.open(sys.stdin.fileno(),'r',encoding='latin-1') as sin:
	for line in sin:

		line = line.strip(' \n\t')
		words = line.split(' ')

		prev = "PEOF"
		prev2 = "PEOF"

		finalresult = ""
		query=""

		if(len(words)>1):
			for i in range(0,len(words)-1):
				pair = words[i].split('/')
			
				query = "prev2_"+prev2+ " prev_"+ prev + " " + pair[0] +" next_"+ words[i+1].split('/')[0]+"\n"
				# print(query)
				result = classify(query,dictofclass)		
				prev2 = prev
				# prev = result[0].decode('utf-8').strip('$')
				prev = result
				finalresult = finalresult + words[i]+"/"+prev +" "
		pair = words[len(words) - 1].split('/')
		
		query ="prev2_"+prev2+ " prev_"+ prev + " " + words[len(words)-1] +" next_"+" next_LEOF"
		# print(query)
		result = classify(query,dictofclass)		
		prev2 = prev
		# prev = result[0].decode('utf-8').strip('$')
		prev = result
		finalresult = finalresult + words[len(words)-1]+"/"+prev +"\n"

		sys.stdout.write(finalresult)
		sys.stdout.flush()
	